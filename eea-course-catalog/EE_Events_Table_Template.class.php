<?php if ( ! defined( 'EVENT_ESPRESSO_VERSION' )) { exit(); }
// define the plugin directory path and URL
define( 'EE_EVENTS_TABLE_TEMPLATE_PATH', plugin_dir_path( __FILE__ ));
define( 'EE_EVENTS_TABLE_TEMPLATE_URL', plugin_dir_url( __FILE__ ));
define( 'EE_EVENTS_TABLE_TEMPLATE_TEMPLATES', EE_EVENTS_TABLE_TEMPLATE_PATH . 'templates' );
/**
 *
 * Class  EE_Events_Table_Template
 *
 * @package			Event Espresso
 * @subpackage		espresso-events-table-template
 * @author			    Seth Shoultes
 *
 */
Class  EE_Events_Table_Template extends EE_Addon {

	/**
	 * class constructor
	 */
	public function __construct() {
		// register our activation hook
		register_activation_hook( __FILE__, array( $this, 'set_activation_indicator_option' ));
	}

	public static function register_addon() {

		// register addon via Plugin API
		EE_Register_Addon::register(
			'Events_Table_Template',
			array(
				'version' 					=> EE_EVENTS_TABLE_TEMPLATE_VERSION,
				'min_core_version' => '4.4.9.rc.034',
				'base_path' 				=> EE_EVENTS_TABLE_TEMPLATE_PATH,
				'main_file_path' 		=> EE_EVENTS_TABLE_TEMPLATE_PATH . 'espresso-events-table-template.php',
				//'admin_callback'	=> 'additional_admin_hooks',
				'autoloader_paths' => array(
					'EE_Events_Table_Template' 	=> EE_EVENTS_TABLE_TEMPLATE_PATH . 'EE_Events_Table_Template.class.php',
				),
				'shortcode_paths' 	=> array( EE_EVENTS_TABLE_TEMPLATE_PATH . 'EES_Course_Catalog.shortcode.php' ),
				// if plugin update engine is being used for auto-updates. not needed if PUE is not being used.
				'pue_options'			=> array(
					'pue_plugin_slug' 		=> 'eea-events-table-view-template',
					'plugin_basename' 	=> EE_EVENTS_TABLE_TEMPLATE_PLUGIN_FILE,
					'checkPeriod' 				=> '24',
					'use_wp_update' 		=> FALSE
				)
			)
		);
	}



	/**
	 * 	additional_admin_hooks
	 *
	 *  @access 	public
	 *  @return 	void
	 */
	public function additional_admin_hooks() {
		// is admin and not in M-Mode ?
		if ( is_admin() && ! EE_Maintenance_Mode::instance()->level() ) {
			add_filter( 'plugin_action_links', array( $this, 'plugin_actions' ), 10, 2 );
		}
	}



	/**
	 * plugin_actions
	 *
	 * Add a settings link to the Plugins page, so people can go straight from the plugin page to the settings page.
	 * @param $links
	 * @param $file
	 * @return array
	 */
	public function plugin_actions( $links, $file ) {
		if ( $file == EE_EVENTS_TABLE_TEMPLATE_PLUGIN_FILE ) {
			// before other links
			array_unshift( $links, '<a href="admin.php?page=espresso_events_table_template">' . __('Settings') . '</a>' );
		}
		return $links;
	}

    public function getEvents($subject, $level, $search_filter, $orderBy)
    {
        global $wpdb;
		$tablename1 = $wpdb->prefix."posts";
        $tablename2 = $wpdb->prefix."ninja_table_items";
		$event_fields = array();
        $query = " SELECT d.value
					FROM `$tablename1` p
					LEFT JOIN `$tablename2` d ON p.ID = d.table_id				
					where p.post_type= 'ninja-table' 
					and p.post_title ='CourseCatalog'
					and JSON_VALUE(value,'$.active') = 1
					and (JSON_VALUE(value,'$.subject') = '$subject' or '$subject' = '')	
					and (LOWER(JSON_VALUE(value,'$.name')) LIKE '%$search_filter%' or 
					     LOWER(JSON_VALUE(value,'$.code')) LIKE '%$search_filter%' or '$search_filter' = '')		
					and (JSON_VALUE(value,'$.level') = '$level' or '$level' = '')" . $orderBy;

        $events = $wpdb->get_results($query, 'ARRAY_A');

		foreach ($events as $event) {
			$fields =  json_decode($event["value"], true);
			$event_fields[] = $fields;
		}
   
        return $event_fields;
    }

	public static function get_level_code($level) {

		$levels = 
		array(      
		  array("level_code"=>"mc1","level"=>"Pre-MathCounts"),
		  array("level_code"=>"mc2","level"=>"AMC 8/MathCounts"),
		  array("level_code"=>"mc3","level"=>"AMC 10/12"),
		  array("level_code"=>"mc4","level"=>"AIME"),
		  array("level_code"=>"mc5","level"=>"USA(J)MO"),
		  array("level_code"=>"cs2","level"=>"Programming"),
		  array("level_code"=>"cc2","level"=>"USACO Bronze"),
		  array("level_code"=>"cc3","level"=>"USACO Silver"),
		  array("level_code"=>"cc4","level"=>"USACO Gold"),
		  array("level_code"=>"cc5","level"=>"USACO Platinum"),
		  array("level_code"=>"fma","level"=>"F=ma"),
		  array("level_code"=>"master","level"=>"Master")
		);
	  
		 $res_levels = array_filter($levels, function ($var) use ($level) {
			 return ($var['level'] === $level);
		 });

		 $result = null;
		 foreach ($res_levels as $res) {
			$result = $res['level_code'];
			break;
		}
		 return $result;
	  }


	  public function get_level($level_code) {

		$levels = 
		array(      
		  array("level_code"=>"mc1","level"=>"Pre-MathCounts"),
		  array("level_code"=>"mc2","level"=>"AMC 8/MathCounts"),
		  array("level_code"=>"mc3","level"=>"AMC 10/12"),
		  array("level_code"=>"mc4","level"=>"AIME"),
		  array("level_code"=>"mc5","level"=>"USA(J)MO"),
		  array("level_code"=>"cs2","level"=>"Programming"),
		  array("level_code"=>"cc2","level"=>"USACO Bronze"),
		  array("level_code"=>"cc3","level"=>"USACO Silver"),
		  array("level_code"=>"cc4","level"=>"USACO Gold"),
		  array("level_code"=>"cc5","level"=>"USACO Platinum"),
		  array("level_code"=>"fma","level"=>"F=ma"),
		  array("level_code"=>"master","level"=>"Master")
		);
	  
		 $res_levels = array_filter($levels, function ($var) use ($level_code) {
			 return ($var['level_code'] === $level_code);
		 });

		 $result = null;
		 foreach ($res_levels as $res) {
			$result = $res['level'];
			break;
		}
		 return $result;
	  }
}
// End of file EE_Events_Table_Template.class.php
// Location: wp-content/plugins/espresso-events-table-template/EE_Events_Table_Template.class.php
